(function() {
  'use strict';

  var RootController = function($state, $stateParams, alerts) {

    var vm = this;

    vm.alerts = alerts.get();

  };

  var LandingController = function($state, $stateParams, alerts) {

    var vm = this;

    //order complete message
    if ($stateParams.msg != null) {
      alerts.add($stateParams.type, $stateParams.msg);
    }

  };

  var StoreController = function($state, $stateParams, alerts, request, checkout, products, cart) {

    var vm = this;

    //inital section parameters
    var section = 1;
    if ($stateParams.section != null) {
      section = $stateParams.section;
    }

    if (section === 1) {
      $state.go('product', { 'pid':'v001' })
    }

    //section navigation
    vm.section = setSection;
    vm.is = isSection;

    //cart functions
    vm.add = addToCart;
    vm.render = renderCart;

    vm.products = products.data;
    vm.adding = false;
    vm.render(cart);

    function setSection (id) {
      section = id;

      if (section === 1) {
        $state.go('product', { 'pid':'v001' })
      }
    };

    function isSection (id) {
      return section == id;
    };

    function addToCart (item) {
      vm.adding = !vm.adding;
      vm.cart.push(item);
      checkout.add(cart);
      vm.render(checkout.get());
      alerts.add('success', item.description + ' has been added to your cart');

      vm.adding = !vm.adding;

    };

    function renderCart (cart) {
      vm.cart = cart;
      vm.cartSize = cart.length;
      vm.cartTotal = checkout.total();
    }

  };

  var ProductController = function($state, $timeout, alerts, checkout, product, cart) {

    var vm = this;

    //cart functions
    vm.add = addToCart;
    vm.order = orderItem;
    vm.render = renderCart;
    vm.toggleImage = toggleImage;

    //product return with regex therefore is returned as array
    vm.item = product.data[0];
    vm.adding = false;
    vm.ordering = false;
    vm.render(cart);

    //merchandise default image
    vm.selectedImage = vm.item.details.image;
    vm.thumbnailImage = vm.item.details.alternate;

    function addToCart (item) {
      vm.adding = !vm.adding;
      checkout.add(item);
      vm.render(checkout.get());
      alerts.add('success', item.description + ' has been added to your cart');

      $timeout(function () {
        vm.adding = !vm.adding;
      }, 1000);

    };

    //go directly to checkout
    function orderItem (item) {
      vm.ordering = !vm.ordering;
      checkout.add(item);
      $state.go('checkout');
      vm.ordering = !vm.ordering;
    };

    function renderCart (cart) {
      vm.cart = cart;
      vm.cartSize = cart.length;
      vm.cartTotal = checkout.total();
    };

    function toggleImage () {
      if (vm.selectedImage === vm.item.details.image) {
        vm.selectedImage = vm.item.details.alternate;
        vm.thumbnailImage = vm.item.details.image;
      } else {
        vm.selectedImage = vm.item.details.image;
        vm.thumbnailImage = vm.item.details.alternate;
      }
    }

  };

  var CartController = function($state, alerts, checkout, cart) {

    var vm = this;

    vm.remove = removeItem;
    vm.render = renderCart;
    vm.checkout = checkoutCart;

    vm.render(cart);

    function removeItem (index) {
      alerts.add('danger', vm.cart[index].description + ' has been removed from your cart.')
      checkout.remove(index);
      vm.render(checkout.get());
    };

    function renderCart (cart) {
      vm.cart = cart;
      vm.cartSize = cart.length;
      vm.cartTotal = checkout.total();
    };

    function checkoutCart () {
      $state.go('checkout');
    };

  };

  var OrderController = function($scope, $state, alerts, request, checkout, cart) {

    var vm = this;

    vm.render = renderCart;

    vm.submtting = false;
    vm.render(cart);

    $scope.handlePayment = function(status, payment) {

      vm.submtting = !vm.submitting;

      if(payment.error) {

        vm.submtting = !vm.submitting;
        alerts.add('danger', 'Sorry ' + vm.order.firstName + ' there was a problem processing your card');

      } else {

        var finalTotal = checkout.total() + 1000;
        var cart = vm.cart;
        var cartString = "";
        for(var i = 0; i <= cart.length; i++) {
          if (cart[i] !== undefined) {
            cartString += cart[i].description + " " + cart[i].product + " ";
            if (cart[i].product === 'merch') {
              cartString += "(" + cart[i].size + ") ";
            }
            cartString += "\n";
          }
        }

        var order = {
          token: payment.id,
          total: finalTotal,
          order: cartString,
          meta: {
            name: vm.order.firstName + ' ' + vm.order.lastName,
            email: vm.order.email,
            address: vm.order.address + ', ' + vm.order.city + ' ' + vm.order.state + ' ' +  vm.order.postcode + ' ' + vm.order.country
          }
        }

        request.add('order', order)
          .then(function(res) {
            if(res.data.type === 'StripeCardError') {

              vm.submtting = !vm.submitting;
              alerts.add('danger', 'Uh Oh! There was a processing error: ' + res.data.message);

            } else {

              vm.submtting = !vm.submitting;
              $state.go('landing', {
                type: 'success',
                msg: 'Your order has been placed your receipt number is: ' + res.data.meta.receipt + ' a copy has been sent to ' + res.data.meta.email
              });
              checkout.empty();

            }
          });

      }
    };

    function renderCart (cart) {
      vm.cart = cart;
      vm.cartSize = cart.length;
      vm.cartTotal = checkout.total();
    };

  };

  var LoginController = function($state, $stateParams, alerts, request) {

    var vm = this;

    //logout & session end msg
    if ($stateParams.msg != null) {
      alerts.add($stateParams.type, $stateParams.msg);
    }

    vm.login = login;

    vm.user = {};
    vm.user.email = 'info@twin.haus';
    vm.submitting = false;

    function login () {

      vm.submitting = !vm.submitting;

      if(vm.user.password !== undefined) {

        request.login(vm.user)
          .then(function(res) {
            if(res.data.success) {

              vm.user = {};
              $state.go('admin');

            } else {

              //login authentication error
              vm.submitting = !vm.submitting;
              alerts.add('danger', 'There was an error signing in: ' + res.data.msg);

            }

          })
          .catch(function(err) {

            //login server error
            vm.submitting = !vm.submitting;
            alerts.add('danger', 'There was an internal server error: ' + err);

          });

        } else {

          //no password entered
          vm.submitting = !vm.submitting;
          alerts.add('danger', 'Enter your password first.');

        }

    };

  };

  var AdminController = function($state, alerts, request, session, orders, products) {

    var vm = this;

    vm.logout = logout;

    vm.user = session.get();
    vm.orders = orders.data;
    vm.products = products.data;

    function logout () {
      session.remove();
      $state.go('login', {
        type: 'success',
        msg: 'You have logged out, fuck you.'
      });
    };

  };

  var Config = function($stateProvider) {

    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        views: {
          root: {
            controller: 'RootController',
            controllerAs: 'vm',
            templateUrl: 'views/base.html'
          }
        }
      })
      .state('landing', {
        url: '/',
        parent: 'root',
        controller: 'LandingController',
        controllerAs: 'vm',
        templateUrl: 'views/landing.html',
        params: {
          type: null,
          msg: null
        }
      })
      .state('store', {
        url: '/store',
        parent: 'root',
        controller: 'StoreController',
        controllerAs: 'vm',
        templateUrl: 'views/store.html',
        params: {
          section: null
        },
        resolve: {
          products: function(request) {
            return request.list('products');
          },
          cart: function(checkout) {
            return checkout.get();
          }
        }
      })
      .state('product', {
        url: '/product/:pid',
        parent: 'root',
        controller: 'ProductController',
        controllerAs: 'vm',
        templateUrl: 'views/product.html',
        resolve: {
          product: function($stateParams, request) {
            return request.get('product', $stateParams.pid);
          },
          cart: function(checkout) {
            return checkout.get();
          }
        }
      })
      .state('cart', {
        url: '/cart',
        parent: 'root',
        controller: 'CartController',
        controllerAs: 'vm',
        templateUrl: 'views/cart.html',
        resolve: {
          cart: function(checkout) {
            return checkout.get();
          }
        }
      })
      .state('checkout', {
        url: '/checkout',
        parent: 'root',
        controller: 'OrderController',
        controllerAs: 'vm',
        templateUrl: 'views/order.html',
        resolve: {
          cart: function(checkout) {
            return checkout.get();
          }
        }
      })
      .state('login', {
        url: '/login',
        parent: 'root',
        controller: 'LoginController',
        controllerAs: 'vm',
        templateUrl: 'views/partials/login.html',
        params: {
          type: null,
          msg: null
        }
      })
      .state('admin', {
        url: '/admin',
        parent: 'root',
        controller: 'AdminController',
        controllerAs: 'vm',
        templateUrl: 'views/partials/admin.html',
        data: {
          requiresLogin: true
        },
        resolve: {
          orders: function(request) {
            return request.list('orders');
          },
          products: function(request) {
            return request.list('products');
          }
        }
      });

  };

  Stripe.setPublishableKey('pk_live_CMMhEUkXrtHcZboHF9HLY673');

  angular
    .module('twinhaus.root', ['angularPayments'])

    .config([
      '$stateProvider',
      Config
    ])

    .controller('RootController', [
      '$state',
      '$stateParams',
      'alerts',
      RootController
    ])

    .controller('LandingController', [
      '$state',
      '$stateParams',
      'alerts',
      LandingController
    ])

    .controller('StoreController', [
      '$state',
      '$stateParams',
      'alerts',
      'request',
      'checkout',
      'products',
      'cart',
      StoreController
    ])

    .controller('ProductController', [
      '$state',
      '$timeout',
      'alerts',
      'checkout',
      'product',
      'cart',
      ProductController
    ])

    .controller('CartController', [
      '$state',
      'alerts',
      'checkout',
      'cart',
      CartController
    ])

    .controller('OrderController', [
      '$scope',
      '$state',
      'alerts',
      'request',
      'checkout',
      'cart',
      OrderController
    ])

    .controller('LoginController', [
      '$state',
      '$stateParams',
      'alerts',
      'request',
      LoginController
    ])

    .controller('AdminController', [
      '$state',
      'alerts',
      'request',
      'session',
      'orders',
      AdminController
    ]);

})();

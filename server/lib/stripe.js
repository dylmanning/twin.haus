'use strict';

const stripe = require('stripe')(process.env.STRIPE_KEY_LIVE);

const stripeController = {

  charge: (obj) => {
    return new Promise((resolve, reject) => {

      stripe.charges.create({
        amount: obj.total,
        currency: 'aud',
        source: obj.token,
        description: obj.order,
        metadata: obj.meta
      }, (err, res) => {
        if (err && err.type === 'StripeCardError') {
          reject(err);
        }
        resolve(res);
      });

    });
  },

  list: () => {
    return new Promise((resolve, reject) => {

      stripe.charges.list({}, (err, res) => {
        (err) ? reject(err) : resolve(res);
      });

    });
  },

  get: (id) => {
    return new Promise((resolve, reject) => {

      stripe.charges.retrieve(id, (err, res) => {
        (err) ? reject(err) : resolve(res);
      });

    });
  },

  update: (id, obj) => {
    return new Promise((resolve, reject) => {

      stripe.charges.update(id, obj, (err, res) => {
        (err) ? reject(err) : resolve(res);
      });

    });
  }

};

module.exports = stripeController;

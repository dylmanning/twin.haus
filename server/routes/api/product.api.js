'use strict';

const mongoose = require('mongoose');

const Product = require('../../models/product.model');

var productController = {

  list: (req, res) => {
    Product.find({}, (err, products) => {
      (err) ? res.json(err) : res.json(products);
    });
  },

  //mongoDB _id is to large for url custom pid's are use so instead of findById RegExp is used
  get: (req, res) => {
    Product.find({ 'pid' : new RegExp('^' + req.params.pid + '$', "i") }, (err, product) => {
      (err) ? res.json(err) : res.json(product);
    })
  },

  update: (req, res) => {
    req.body.updated = Date.now();
    Product.findByIdAndUpdate(req.params.id, req.body, (err, product) => {
      (err) ? res.json(err) : res.sendStatus(200);
    });
  }

};

module.exports = productController;

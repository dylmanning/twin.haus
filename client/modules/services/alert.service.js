(function() {
  'use strict';

  var alertService = function($timeout) {

    var srv = this;
    var alerts = [];

    srv.add = function(type, msg) {

      var alert = {
        type: type,
        msg: msg,
        close: function() {
          return closeAlert(this);
        }
      };

      $timeout(closeAlert, 5500, true, alert);
      return alerts.push(alert);

    };

    srv.get = function() {
      return alerts;
    };

    srv.clear = function() {
      alerts = [];
    };

    function closeAlert(alert) {
      return closeAlertIdx(alerts.indexOf(alert));
    }

    function closeAlertIdx(index) {
      return alerts.splice(index, 1);
    }

  };

  angular
    .module('twinhaus')
    .service('alerts', [
      '$timeout',
      alertService
    ]);

})();

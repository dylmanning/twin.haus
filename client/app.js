(function() {
  'use strict';

  angular
    .module('twinhaus', [
      'ngAnimate',
      'ui.router',
      'ui.bootstrap',
      'angular-jwt',
      'angular-storage',
      'twinhaus.root'
    ])

    .config([
      '$urlRouterProvider',
      'jwtInterceptorProvider',
      '$httpProvider',
      '$locationProvider',
      function($urlRouterProvider, jwtInterceptorProvider, $httpProvider, $locationProvider) {

        $urlRouterProvider.otherwise('/');
        jwtInterceptorProvider.tokenGetter = function(store) {
          return store.get('jwt');
        }
        $httpProvider.interceptors.push('jwtInterceptor');
        $locationProvider.html5Mode(true);

      }
    ])

    .run(function($rootScope, $state, store, jwtHelper) {

      //authentication
      $rootScope.$on('$stateChangeStart', function(e, to) {
        if (to.data && to.data.requiresLogin) {
          if (!store.get('jwt') || jwtHelper.isTokenExpired(store.get('jwt'))) {
            e.preventDefault();
            $state.go('login', {
              type: 'warning',
              msg: 'You are not logged in, so login.'
            });
          }
        }
      });

    });

})();

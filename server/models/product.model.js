'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
  pid: String,
  product: String,
  added: Date,
  description: {
    type: String,
    required: true
  },
  details: {
    artist: String,
    stock: String,
    mount: String,
    series: String,
    size: String,
    material: String,
    style: String,
    model: String,
    link: String,
    image: String,
    alternate: String,
    thumbnail: String
  },
  cost: Number,
  inventory: Number,
  inStock: {
    type: Boolean,
    default: true
  }
});

module.exports = mongoose.model('Product', productSchema);

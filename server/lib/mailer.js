'use strict';

const sendgrid = require('sendgrid')('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');

const mailerController = {

  send: (obj) => {

    let total = obj.total / 100;
    let destination = obj.meta.email;
    let subject = 'Your order from Twin Haus';
    let html = [
      '<div style="text-align: center;">',
      '    <h1>Your order has been placed</h1>',
      '    <p><strong>', obj.order, '</strong></p>',
      '    <p>$', total, '</p>',
      '    <p>', obj.meta.receipt, '</p>',
      '</div>'
    ].join('');

    let msg = new sendgrid.Email();
    msg.addTo(destination);
    msg.setSubject(subject);
    msg.setFrom('info@twin.haus');
    msg.setHtml(html);
    msg.addFilter('templates', 'enable', 1);
    msg.addFilter('templates', 'template_id', '26934373-b2b5-42ad-8726-e0215a711463');
    sendgrid.send(msg, function(err, docs) {
      if (err) { return console.error(err); }
      if (!err) {}
    });

  }

};

module.exports = mailerController;

'use strict';

const dotenv = require('dotenv').config();
const express = require('express');

let port = process.env.NODE_PORT || 3000;
let host = process.env.NODE_HOST || 'localhost';

const app = express();

const server = app.listen(port, host, () => {
  console.log('\x1b[36m%s\x1b[0m', '[twin.haus]: listening on localhost:3000');
});

require('./init/')(app);
require('./routes/')(app);

//seed database
if(process.argv[2]) {
  console.log('\x1b[35m%s\x1b[0m', 'seeding database');
  require('./init/seed');
}

//expose app
exports = module.exports = app;

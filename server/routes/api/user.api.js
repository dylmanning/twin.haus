'use strict';

const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = require('../../models/user.model');

const userController = {

  list: (req, res) => {
    User.find({}, (err, users) => {
      (err) ? console.error(err) : res.json(users);
    });
  },

  get: (req, res) => {
    User.findById(req.params.id, (err, user) => {
      (err) ? console.error(err) : res.json(user);
    });
  },

  update: (req, res) => {
    req.body.updated = Date.now();
    User.findByIdAndUpdate(req.params.id, req.body, (err, user) => {
      (err) ? console.error(err) : res.sendStatus(200);
    });
  },

  login: (req, res) => {
    User.findOne({ email: req.body.email }, (err, user) => {
      if (err) {
        //testing error checking
        console.log(err);
        throw err;
      }
      (user) ? userFound(req, res, user) : userNotFound(res);
    });
  }

};

const generateToken = (user) => {
  let payload = {
    iss: 'twin.haus',
    sub: user
  };
  return jwt.sign(payload, process.env.TOKEN_SECRET, { 'expiresIn': '24h' });
};

const userNotFound = (res) => {
  res.send({ success: false, msg: 'User does not exsist.' });
};

const userFound = (req, res, user) => {
  user.comparePassword(req.body.password, (err, isMatch) => {
    if (isMatch && !err) {
      res.json(Object.assign({ success: true, token: generateToken(user) }, user));
    } else {
      res.send({ success: false, msg: 'Incorrect password' });
    }
  });
};

module.exports = userController;

'use strict';

const Stripe = require('../../lib/stripe');
const Mailer = require('../../lib/mailer');

const uuid = require('node-uuid');


const orderController = {

  list: (req, res) => {

    Stripe.list()
      .then((orders) => { res.json(orders); })
      .catch((err) => { res.json(err); });

  },

  get: (req, res) => {

    Stripe.get(req.body.id)
      .then((order) => { res.json(order); })
      .catch((err) => { res.json(err); });

  },

  update: (req, res) => {

    Stripe.update(req.body.id)
      .then((order) => { res.json(order); })
      .catch((err) => { res.json(err); });

  },

  add: (req, res) => {

    let response = res;

    req.body.meta.receipt = uuid.v4();

    Stripe.charge(req.body)
      .then((res) => {
        if (res.status === 'succeeded') {

          //return receipt number
          response.json(req.body);

          //dispatch client receipt
          Mailer.send(req.body);

          //update product in inventory
          // let obj = req.body.order;
          //
          // for(let i = 0; i < obj.length; i++) {
          //
          //   let _id = obj[i]._id;
          //   let _inventory = obj[i].inventory--;
          //   let _inStock = inStock(obj[i].inventory);
          //
          //   Product.findByIdAndUpdate(_id, {$set: {inventory: _inventory, inStock: _inStock}}, (err) => {
          //     if (err) {
          //       console.error(err);
          //     }
          //     if (!err) {
          //
          //       if(_inStock) {
          //         console.log('\x1b[32m%s\x1b[0m', getTimestamp() + ' ',  obj[i].description + ' was ordered. ' + _inventory + ' copies left');
          //       } else {
          //         console.log('\x1b[32m%s\x1b[0m', getTimestamp() + ' ', obj[i].description + ' sold out.');
          //       }
          //
          //     }
          //   });
          //
          // }

        }
      })
      .catch((err) => {
        response.json(err);
      });

    function inStock(inventory) {
      return inventory > 0;
    };

    function getTimestamp() {
      let now = new Date();
      let date = '[' + now.getUTCMonth() + '-' + now.getUTCDate() + '-' + now.getUTCFullYear() + ' ';
      let h = date.getHours(), m = date.getMinutes();
      let time = (h > 12) ? (h-12 + ':' + m +'PM]') : (h + ':' + m +'AM]');
      return date + time;
    };

  }

};

module.exports = orderController;

-'use strict';

const User = require('../models/user.model');
const Product = require('../models/product.model');

//set user accounts
User.find({}).remove(() => {

  //admin account
  User.create({
    email: 'info@twin.haus',
    password: 'password',
    firstName: 'Daniel',
    lastName: 'Grima',
    joined: Date.now()
  });

});

//set products
Product.find({}).remove(() => {

  //vinyl
  Product.create({
    pid: 'v001',
    description: 'Nothing Lavish EP',
    product: 'vinyl',
    details: {
      image: '../assets/img/v001_1.jpg'
    },
    cost: 3500,
    inventory: 100,
    added: Date.now()
  });

  //artworks
  Product.create({
    pid: 'a001',
    description: 'Synthetic Egg',
    product: 'artwork',
    details: {
      artist: 'Yuli Serfaty',
      size: '40x52cm',
      stock: 'Cotton Rag Eggshell Archival Grade stock',
      mount: 'Palm White Gallery Mat',
      series: 'first',
      image: '../assets/img/a001_1.jpg',
      thumbnail: '../assets/img/a001_2.jpg',
      link: 'http://yuliserfaty.com/'
    },
    cost: 3500,
    inventory: 10,
    added: Date.now()
  });

  Product.create({
    pid: 'a002',
    description: 'Self-love',
    product: 'artwork',
    details: {
      artist: 'Adrianna Mammino',
      size: '40x56cm',
      stock: 'White Linen Archival Grade stock',
      mount: 'Arctic White Gallery Mat',
      series: 'second',
      image: '../assets/img/a002_1.jpg',
      thumbnail: '../assets/img/a002_2.jpg',
      link: 'https://adriannamammino.bigcartel.com/'
    },
    cost: 3500,
    inventory: 10,
    added: Date.now()
  });

  Product.create({
    pid: 'a003',
    description: 'I Used to Think',
    product: 'artwork',
    details: {
      artist: 'Sophie Hopkins',
      size: '40x52cm',
      stock: 'White Linen Archival Grade stock',
      mount: 'Ivory Gallery Mat',
      series: 'third',
      image: '../assets/img/a003_1.jpg',
      thumbnail: '../assets/img/a003_2.jpg',
      link: 'https://drinkyourfuckingmilk.tumblr.com/'
    },
    cost: 3500,
    inventory: 10,
    added: Date.now()
  });

  Product.create({
    pid: 'a004',
    description: 'The Revue',
    product: 'artwork',
    details: {
      artist: 'Marija Kozomora',
      size: '40x53cm',
      stock: 'Cotton Rag Eggshell Archival Grade stock',
      mount: 'Mottle Ivory Gallery Mat',
      series: 'final',
      image: '../assets/img/a004_1.jpg',
      thumbnail: '../assets/img/a004_2.jpg',
      link: 'https://www.instagram.com/marija.kozomora/'
    },
    cost: 3500,
    inventory: 10,
    added: Date.now()
  });

  //merchandise
  Product.create({
    pid: 'm001',
    description: 'Neuroendoscopy',
    product: 'merch',
    details: {
      style: 'Unisex White S/S Tee',
      material: '100% Organic Combed Cotton',
      model: 'Model wears size M / 5"10',
      artist: 'Brayden Doig',
      image: '../assets/img/m001_1.jpg',
      alternate: '../assets/img/m001_2.jpg',
      link: 'http://www.doigsmind.com/'
    },
    cost: 2500,
    inventory: 10
  });

  Product.create({
    pid: 'm002',
    description: 'Nothing Lavish',
    product: 'merch',
    details: {
      style: 'Unisex Navy L/S Tee',
      material: '200 GSM 100% Carded Cotton',
      model: 'Model wears size M / 5"10',
      artist: 'Madeline Holt',
      image: '../assets/img/m002_1.jpg',
      alternate: '../assets/img/m002_2.jpg',
      link: 'https://www.instagram.com/madelineholt/'
    },
    cost: 3000,
    inventory: 10
  });

});

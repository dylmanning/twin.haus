(function() {
  'use strict';

  var sessionService = function(store) {

    var srv = this;

    srv.get = function() {
      return store.get('user');
    };

    srv.set = function(token, user) {
      store.set('user', user);
      store.set('jwt', token);
    };

    srv.remove = function() {
      store.remove('jwt');
      store.remove('user');
    };

  };

  angular
    .module('twinhaus')
    .service('session', [
      'store',
      sessionService
    ]);

})();

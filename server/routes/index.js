'use strict';

const path = require('path');

const api = {
	authentication: require('./api/authenticate.api'),
	user: require('./api/user.api'),
	orders: require('./api/order.api'),
	products: require('./api/product.api')
};

module.exports = (app) => {

	//authenticate user
	app.post('/api/login', api.user.login);

  //user
	app.get('/api/users', api.authentication.verify, api.user.list);
  app.get('/api/user/:id', api.authentication.verify, api.user.get);
  app.put('/api/user/:id', api.authentication.verify, api.user.update);

  //orders
	app.post('/api/order', api.orders.add);

	app.get('/api/orders', api.authentication.verify, api.orders.list);
  // app.get('/api/order/:id', api.authentication.verify, api.orders.get);
  // app.put('/api/order/:id', api.authentication.verify, api.orders.update);
  // app.delete('/api/bean/:id', api.authentication.verify, api.orders.remove);

	//products
	app.get('/api/products', api.products.list);
	app.get('/api/product/:pid', api.products.get);

	//fallback routes
	app.get('/', (req, res) => {
	  res.sendFile(path.join(__dirname, '/../../client/index.html'));
	});

	app.get('*', (req, res) => {
		res.redirect('/#' + req.originalUrl);
	});

};

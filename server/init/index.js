'use strict';

const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

module.exports = function(app) {

  // var uri = 'mongodb://user:pass@host:port/db';
  var uri = 'mongodb://' + process.env.DB_HOST + '/' + process.env.DB_NAME ;

  mongoose.connect(uri);
  var conn = mongoose.connection;

  app.use(express.static(path.join(__dirname + '/../../client')));

  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.use(bodyParser.json());
  app.use(cookieParser());

  app.use((req, res, next) => {

    req.isAuthenticated = () => {
      var token = (req.headers.authorization && req.headers.authorization.split(' ')[1]) || req.cookies.token;
      try {
        return jwt.verify(token, process.env.TOKEN_SECRET);
      } catch (err) {
        return false;
      }
    };

    if (req.isAuthenticated()) {
      var payload = req.isAuthenticated();
      User.findById(payload.sub, function(err, user) {
        req.user = user;
        next();
      });
    } else {
      next();
    }

  });

};

(function() {
  'use strict';

  var requestService = function($http, session) {

    var srv = this;

    srv.login = function(user) {
      return $http.post('/api/login', user)
        .success(function(res) {
          if (res.success) {
            session.set(res.token, res._doc);
          }
        })
        .error(function(err) {
          return err;
        });
    };

    srv.list = function(api) {
      return $http.get('/api/' + api)
    };

    srv.get = function(api, id) {
      return $http.get('/api/' + api + '/' + id);
    };

    srv.add = function(api, obj) {
      return $http.post('/api/' + api, obj);
    };

    srv.update = function(api, id, obj) {
      return $http.put('/api/' + api + '/' + id, obj);
    };

    srv.remove = function(api, arr) {
      return $http.delete('/api/' + api + '/' + arr);
    };

  };

  angular
    .module('twinhaus')
    .service('request', [
      '$http',
      'session',
      requestService
    ]);

})();

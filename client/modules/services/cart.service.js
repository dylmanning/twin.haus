(function() {
  'use strict';

  var cartService = function(store) {

    var srv = this;

    //if no cart intitate empty cart
    if(!store.get('cart')) {
      store.set('cart', []);
    }

    srv.get = function() {
      return store.get('cart');
    };

    srv.add = function(item) {

      var cart = store.get('cart');

      cart.push(item);
      store.set('cart', cart);
    };

    srv.remove = function(index) {

      var cart = store.get('cart');

      if(cart.length < 2) {
        cart = [];
      } else {
        cart.splice(index, 1);
      }
      store.set('cart', cart);
    };

    srv.total = function() {

      var cart = store.get('cart');
      var total = 0;

      for(var i = 0; i < cart.length; i++) {
        total = total + cart[i].cost;
      }

      return total;
    };

    srv.empty = function() {
      store.set('cart', []);
    };

  };

  angular
    .module('twinhaus')
    .service('checkout', [
      'store',
      cartService
    ]);

})();

'use strict';

const path = require('path');

const User = require('../../models/user.model');

const authenticationController = {
  verify: (req, res, next) => {
    if(req.isAuthenticated()) {
      next();
    } else {
      console.log(req.headers.host + ' has tried to access a restricted API');
      res.status(401).sendFile(path.join(__dirname + '/../../../client/views/partials/401.html'));
    }
  }
};

module.exports = authenticationController;
